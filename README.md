# Prantls-Bakery
This website was part of a static web design/development project for 67-250. The goal of the project was to develop a restructuring of the current [Prantls Bakery site](http://www.prantlsbakery.com/).

**The project is deployed [here](https://liao-frank.github.io/Prantls-Bakery/) through ghpages.**
