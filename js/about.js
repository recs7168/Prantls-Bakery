$(document).ready(function() {
    $( ".yelp-logo" ).mouseout(function(){
        $(this).attr("src", "images/logos/yelp.png");
    });

    $( ".yelp-logo" ).mouseover(function(){
        $(this).attr("src", "images/logos/yelp-hover.png");
    });

    $( ".the-knot-logo" ).mouseout(function(){
        $(this).attr("src", "images/logos/the-knot.png");
    });

    $( ".the-knot-logo" ).mouseover(function(){
        $(this).attr("src", "images/logos/the-knot-hover.png");
    });

    $( ".trip-advisor-logo" ).mouseout(function(){
        $(this).attr("src", "images/logos/trip-advisor.png");
    });

    $( ".trip-advisor-logo" ).mouseover(function(){
        $(this).attr("src", "images/logos/trip-advisor-hover.png");
    });

});