$(document).ready(function() {
    $( ".nav-center" ).mouseover(function(){
        $("#nav-logo").attr("src", "images/prantl-icon-shaded.png");
        $(".nav-center a").attr("style", "font-weight: bold; color: #009359;")
    });

    $( ".nav-center" ).mouseout(function(){
        $("#nav-logo").attr("src", "images/prantl-icon.png");
        $(".nav-center a").attr("style", "font-weight: normal; color: #00a665;")
    });
});