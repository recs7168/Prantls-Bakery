$(document).ready(function() {
	// Social Media hover functions
    $( "#facebook" ).mouseover(function(){
        $("#facebook").attr("src", "images/facebook-hover.png");
    });

    $( "#facebook" ).mouseout(function(){
        $("#facebook").attr("src", "images/facebook.png");
    });

    $( "#twitter" ).mouseover(function(){
        $("#twitter").attr("src", "images/twitter-hover.png");
    });

    $( "#twitter" ).mouseout(function(){
        $("#twitter").attr("src", "images/twitter.png");
    });

    $( "#instagram" ).mouseover(function(){
        $("#instagram").attr("src", "images/instagram-hover.png");
    });

    $( "#instagram" ).mouseout(function(){
        $("#instagram").attr("src", "images/instagram.png");
    });
});