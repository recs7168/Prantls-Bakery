$(document).ready(function() {
    $( ".right-slides-control" ).mouseover(function(){
        $(".right-slides-arrow").attr("src", "images/arrow-green.png");
    });

    $( ".right-slides-control" ).mouseout(function(){
        $(".right-slides-arrow").attr("src", "images/arrow-dark-brown.png");
    });

    $( ".left-slides-control" ).mouseover(function(){
        $(".left-slides-arrow").attr("src", "images/arrow-green.png");
    });

    $( ".left-slides-control" ).mouseout(function(){
        $(".left-slides-arrow").attr("src", "images/arrow-dark-brown.png");
    });

});

function validate(){
	var number = parseInt($("#zipCode").val());
	if(isNaN(number) || number < 10000 || number > 99999) {
		alert("Zip code is not valid or not in a valid format.");
		return false;
	}
	return true;
}